﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

namespace Client
{

    class Network
    {
        public TcpClient Client;
        public string SentMassage;
        public string ReceiveMassage;
        void Send()
        {
            while (Client.Connected)
            {
                if (SentMassage != "")
                {
                    byte[] Massege = System.Text.Encoding.ASCII.GetBytes(SentMassage);
                    Client.Client.Send(Massege);
                    SentMassage = "";
                }
            }
        }
        void Receive()
        {
            while (Client.Connected)
            {
                byte[] Byte = new byte[1000];
                Client.Client.Receive(Byte);
                string Result = System.Text.Encoding.ASCII.GetString(Byte);
                int Lenght = 0;
                for (int i = 0; Byte[i] != 0; i++)
                {
                    Lenght++;
                }
                ReceiveMassage = Result.Substring(0, Lenght);
            }
        }
        public void Connect()
        {
            ReceiveMassage = "";
            IPAddress IpAddress = IPAddress.Parse("127.0.0.1");
            int Port = 8000;
            Client = new TcpClient();
            while (!Client.Connected)
            {
                try
                {
                    Client.Connect(IpAddress, Port);
                }
                catch { }
                if(Client.Connected)
                {
                    SentMassage = "Connect start";


                    Task Send = new Task(this.Send);
                    Send.Start();
                    Task Receive = new Task(this.Receive);
                    Receive.Start();
                }
            }
        }

    }
}
