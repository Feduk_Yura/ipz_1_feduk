﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Form1 : Form
    {
        Network net;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            net = new Network();
            Task Connect = new Task(net.Connect);
            Connect.Start();

        }

        private void StudentsList_Click(object sender, EventArgs e)
        {
            try
            {
                listBox1.Items.Clear();
                while (!net.Client.Connected) { }
                net.SentMassage = "List";
                while (net.ReceiveMassage == "") { }
                string ReceiveMassage = net.ReceiveMassage;
                net.ReceiveMassage = "";
                int a = 0;
                int position = 0;
                for (int i = 0; i < ReceiveMassage.Length; i++)
                {
                    if (ReceiveMassage[i] == '\n')
                    {

                        listBox1.Items.Insert(position, ReceiveMassage.Substring(a, i - a));
                        position++;
                        a = i + 1;
                    }
                }
                listBox1.SelectedIndex = 0;
            }
            catch { };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(listBox1.SelectedItem);
            net.SentMassage = "Delete";
            while (net.SentMassage != "") { }
            net.SentMassage = Convert.ToString(listBox1.SelectedItem);
            while (net.ReceiveMassage == "") { }
            string ReceiveMassage = net.ReceiveMassage;
            int a = 0;
            int position = 0;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (StudentName.Text != "")
            {
                listBox1.Items.Clear();
                net.SentMassage = "Seach";
                while (net.SentMassage != "") { }
                net.SentMassage = StudentName.Text;
                while (net.ReceiveMassage == "") { }
                string ReceiveMassage = net.ReceiveMassage;
                net.ReceiveMassage = "";
                int a = 0;
                int position = 0;
                for (int i = 0; i < ReceiveMassage.Length; i++)
                {
                    if (ReceiveMassage[i] == '\n')
                    {

                        listBox1.Items.Insert(position, ReceiveMassage.Substring(a, i - a));
                        position++;
                        a = i + 1;
                    }
                }
                listBox1.SelectedIndex = 0;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
