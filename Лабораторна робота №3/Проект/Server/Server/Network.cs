﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

namespace Server
{
    
    class Network
    {
        public TcpClient Client;
        public string ReceiveMassage;
        public void Send(string SentMassage)
        {
            byte[] Massege = System.Text.Encoding.ASCII.GetBytes(SentMassage);
            Client.Client.Send(Massege);
            Console.WriteLine("Ответ: \n" + SentMassage);
            SentMassage = "";
        }
        void Receive()
        {
            while (Client.Connected)
            {
                byte[] Byte = new byte[1000];
                Client.Client.Receive(Byte);
                string Result = System.Text.Encoding.ASCII.GetString(Byte);
                int Lenght = 0;
                for (int i = 0; Byte[i] != 0; i++)
                {
                    Lenght++;
                }
                ReceiveMassage = Result.Substring(0, Lenght);
                Console.WriteLine("Запрос:  \n"+ReceiveMassage);
            }
        }
        public void Connect()
        {
            IPAddress IpAddress = IPAddress.Parse("127.0.0.1");
            int Port = 8000;
            TcpListener listener = new TcpListener(IpAddress, Port);
            listener.Start();
            while(true)
            {
                Client = listener.AcceptTcpClient();


                Task Receive = new Task(this.Receive);
                Receive.Start();
            }
        }

        public void RunServer()
        {
            Task Connect = new Task(this.Connect);
            Connect.Start();
        }
    }
}
